<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Complete Bootstrap 4 Website Layout</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora|Open+Sans|Source+Sans+Pro" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-md navbar-light myNavbar">
    <div class="container-fluid ">
        <a class="navbar-brand" href="#"><img src="img/Logo.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li><li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li><li class="nav-item">
                    <a class="nav-link" href="#">Brands</a>
            </ul>
        </div>
    </div>
</nav>

<!--- Image Slider -->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block" src="img/background3.png" alt="First slide">
            <div class="carousel-caption">
                <h1>Velox One Page Template</h1>
                <h3>Clean, Responsive, customizable one page Template</h3>
                <button type="button" class="btn btn-GetStartedNow btn-primary btn-lg">Get Started Now</button>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block" src="img/background3.png" alt="Second slide">
            <div class="carousel-caption">
                <h1>Velox One Page Template</h1>
                <h3>Clean, Responsive, customizable one page Template</h3>
                <button type="button" class="btn btn-GetStartedNow btn-primary btn-lg">Get Started Now</button>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block" src="img/background3.png" alt="Second slide">
            <div class="carousel-caption">
                <h1>Velox One Page Template</h1>
                <h3>Clean, Responsive, customizable one page Template</h3>
                <button type="button" class="btn btn-GetStartedNow btn-primary btn-lg">Get Started Now</button>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon sliderArrows"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon sliderArrows"></span>
        <span class="sr-only">Next</span>
    </a>
</div>